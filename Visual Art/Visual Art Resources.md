---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-11-20
tags:
- visual-art
- meta
---


# Visual Art Resources

## Pixel Art

### YouTube Chanels

- [AdamCYounis](https://www.youtube.com/@AdamCYounis)
- [Pixel Architect](https://www.youtube.com/@PixelArchitect)
- [t3ssel8r](https://www.youtube.com/@t3ssel8r)

## Animation

### YouTube Chanels

- [New Frame Plus](https://www.youtube.com/@NewFramePlus)
