"""
a small script that checks if links are falsely generated 
with the folder name 'puplic' which should be ommited.
"""

import os

def fix(path: str):
    for elem in os.listdir(path):
        elem_path = f"{path}{os.sep}{elem}"
        print(elem_path)
        if os.path.isdir(elem_path):
            fix(elem_path)
        if elem.endswith(".html"):
            with open(elem_path, encoding="utf-8") as file:
                content = file.read()
            fixed = content.replace("href=\"public/","href=\"")
            with open(elem_path, "w", encoding="utf-8") as file:
                file.write(fixed)
            

if __name__ == "__main__":
    fix(os.sep.join(__file__.split(os.sep)[:-2] + ["public"]))    