"""
script to update the index page with up to date values for internal and external links
"""

import os

def count_links(path: str) -> tuple[int, int]:
    """:returns: (internal_count, external_count)"""
    internal = 0
    external = 0

    for elem in os.listdir(path):
        elem_path = f"{path}{os.sep}{elem}"
        if os.path.isdir(elem_path):
            temp_in, temp_ext = count_links(elem_path)
            internal += temp_in
            external += temp_ext
        if elem.endswith(".html"):
            print(elem_path)
            with open(elem_path, encoding="utf-8") as file:
                content = file.read()
            external += content.count('class="external-link"')
            internal += 1

    return internal, external

def replace_link_counts(path:str, internal, external):
    """replaces the counts in the index file"""
    with open(path, encoding="utf-8") as file:
        content = file.read()
    temp = content.find("currently tracking more than ") + 29  # len of the str
    prev = content[:temp]
    after = content[temp:]
    temp = after.find("</strong>")
    middle = f"<strong>{external}"
    after = after[temp:]
    temp = after.find("<strong>") + 8
    ignore = after[:temp]
    after = after[temp:]
    pos = after.find("</strong>")
    final = str(internal) + after[pos:]

    new_content = prev + middle + ignore + final

    with open(path, encoding="utf-8", mode="w") as file:
        content = file.write(new_content)


if __name__ == "__main__":
    public_root = os.sep.join(__file__.split(os.sep)[:-2] + ["public"])
    counts = count_links(public_root)
    replace_link_counts(f"{public_root}{os.sep}index.html", *counts)
