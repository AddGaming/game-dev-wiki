---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-11-13
tags: 
- software
- organization
- git
- missing
---

# GitLab

[[GitLab]] is an open source [[DevOps]] platform which provides a cloud storage for [[Git]] as well as management tools for software projects.
The software is following a freemium pricing model and can be self hosted.

## Git Integration

## Management Tools

## Integrations

## Hosting

While [[GitLab]] offers to host it's service for you, you can host your own [[GitLab]] instance.


_____

## Sources

- [GitLab - Website](https://about.gitlab.com/)
