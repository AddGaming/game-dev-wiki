---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-26
tags: 
- software
- programming
- Windows
- Linux
- MacOS
- IDE
- missing
primary-languages:
- Java
- Kotlin
---

# IntelliJ

[[IntelliJ]] is a [[IDE]] developed by [Jetbrains](https://www.jetbrains.com/) that focuses on developing software for the  [[JVM]].

*content*

## Features

| Feature                     | Present |
| --------------------------- | ------- |
| File Explorer               | ✅      |
| VCS integration             | ✅      |
| GitHub integration          | ✅      |
| Linter                      | ✅      |
| Package Manager             | ❓      |
| Refactoring Tools           | ✅      |
| Doc-String generation       | ✅      |
| IntelliSense                | ✅      |
| Debugger                    | ✅      |
| Multiple Execution Profiles | ✅      |
| Profiler                    | ✅      |
| Database Inspector          | ✅      |
| Syntax Highlighting         | ✅      |
| Code Formatter              | ✅      |
| Code Folding                | ✅      |
| Vim Bindings                | ✅      |
| Code Templating             | ✅      |
| Goto Definition             | ✅      |
| Decompiler                  | ✅      |
| Find Usages                 | ✅      |
| Code Coverage Analysis      | ✅      |
| Build System                | ✅      |
| Build System Integration    | ✅      |
| Extensible with Plugins     | ✅      | 

## Supported VCSs

- Git
- SVN
- Mercurial
- Perforce

## List of Refactoring Tools

| Feature                    | Present |
| -------------------------- | ------- |
| Extract to Method          | ✅      |
| Smart Delete               | ✅      |
| Rename                     | ✅      |
| Extract to Class           | ✅      |
| Change Signature           | ✅      |
| Extract to Variable        | ✅      |
| Extract to Constant        | ✅      |
| Extract to Field           | ✅      |
| Extract to Parameter       | ✅      |
| Move Code to File          | ✅      |
| Create Interface for Class | ❌      |


_____

## Sources

- [IntelliJ - Website](https://www.jetbrains.com/idea/)
