---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-23
tags: 
- software
- programming
- Windows
- Linux
- MacOS
- IDE
primary-languages:
- Python
---

# PyCharm

[[PyCharm]] is a [[IDE]] developed by [Jetbrains](https://www.jetbrains.com/) that focuses on developing software with [[Python]].
The [[IDE]] integrates with almost all tools prevalent in the [[Python]] ecosystem.
The software is available in two versions. 
A **free** `community` version and a **paid** `professional` version.
Both versions can be [downloaded here](https://www.jetbrains.com/pycharm/download/other.html).
The professional version differs mainly in the amount of supported languages and available package managers.
A full comparison can be [viewed here](https://www.jetbrains.com/products/compare/?product=pycharm&product=pycharm-ce).
Jetbrains also offers educational licenses for students and teachers to use the `professional` version for **free**.

## Features

| Feature                     | Present |
| --------------------------- | ------- |
| File Explorer               | ✅      |
| VCS integration             | ✅      |
| GitHub integration          | ✅      |
| Linter                      | ✅      |
| Package Manager             | ✅      |
| Refactoring Tools           | ✅      |
| Doc-String generation       | ✅      |
| IntelliSense                | ✅      |
| Debugger                    | ✅      |
| Multiple Execution Profiles | ✅      |
| Profiler                    | ✅      |
| Database Inspector          | ✅      |
| Syntax Highlighting         | ✅      |
| Code Formatter              | ✅      |
| Code Folding                | ✅      |
| Vim Bindings                | ✅      |
| Code Templating             | ✅      |
| Goto Definition             | ✅      |
| Decompiler                  | ✅      |
| Find Usages                 | ✅      |
| Code Coverage Analysis      | ✅      |
| Build System                | ❌      |
| Build System Integration    | ❌      | 
| Extensible with Plugins     | ✅      | 

## Supported VCSs

- Git
- SVN
- Mercurial
- Perforce

## Supported Package-Manager

- Conda
- Virtualenv
- Pipenv
- Poetry

## List of Refactoring Tools

| Feature                    | Present |
| -------------------------- | ------- |
| Extract to Method          | ✅      |
| Smart Delete               | ✅      |
| Rename                     | ✅      |
| Extract to Class           | ✅      |
| Change Signature           | ✅      |
| Extract to Variable        | ✅      |
| Extract to Constant        | ✅      |
| Extract to Field           | ✅      |
| Extract to Parameter       | ✅      |
| Move Code to File          | ✅      |
| Create Interface for Class | ❌      |

_____

## Sources

- [PyCharm - Website](https://www.jetbrains.com/pycharm/)
