---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-20
tags: 
- software
- IDE
- programming
- missing
---

# Integrated Development Environment

*definition*


## List of Text Editors

```dataview
table
choice(contains(row.tags, "Windows"), "✅", "❌") as Windows, 
choice(contains(row.tags, "MacOS"), "✅", "❌") as MacOs, 
choice(contains(row.tags, "Linux"), "✅", "❌") as Linux,
join(row.primary-languages, ", ") as "Primary Langages"
from #IDE 
where !contains(file.path, this.file.path)
sort length(row.tags) desc 
```

_____

## Sources
