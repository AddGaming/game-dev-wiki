---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-23
tags: 
- software
- programming
- IDE
- Windows
- missing
---

# Visual Studio

[[Visual Studio]] is an [[IDE]] developed by [Microsoft](https://www.microsoft.com/de-de/).



_____

## Sources

- [Visual Studio - Website](https://visualstudio.microsoft.com/de/)
