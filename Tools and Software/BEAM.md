---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-11-15
tags: 
- software
- interpreter
- BEAM
- missing
---

# BEAM

The [[BEAM]] (_Björn's Erlang Abstract Machine_) is the core [[Code interpretation|interpreter]] for the [[Erlang]] programming language.
It executes [[Byte Code]] that is also the compilation target of other languages.

## List of BEAM languages

```dataview
list
from #BEAM
where file.name != this.file.name
```

_____

## Sources

- [BREAM - Wikipedia](https://en.wikipedia.org/wiki/BEAM_(Erlang_virtual_machine))
- [BEAM languages list - llaisdy](https://github.com/llaisdy/beam_languages#34-languages-on-the-beam)
- [YouTube - The Erlang Ecosystem - Robert Virding](https://www.youtube.com/watch?v=7AJR66p5E4s)
