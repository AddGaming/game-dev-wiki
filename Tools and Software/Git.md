---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-11-23
tags: 
- software
- vcs
- git
- missing
---

# Git

[[Git]] is a open source [[Version Control System]] that is the most popular choice of programmers.

_____

## Sources

- [Git - Website](https://git-scm.com/)
