---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-26
tags: 
- software
- interpreter
- jvm
- missing
---

# Java Virtual Machine

*content*

# Languages using the JVM

```dataview
list
from #jvm 
where file.name != this.file.name
```

_____

## Sources
