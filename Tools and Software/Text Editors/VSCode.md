---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-23
tags: 
- software
- text-editor
- missing
---

# Visual Studio Code

[[VSCode|Visual Studio Code]] (alt. *VSCode*  or *VS Code*) is a [[Text Editor|text editor]] developed by [Microsoft](https://www.microsoft.com/de-de/). 



_____

## Sources

- [Visual Studio Code - Website](https://code.visualstudio.com/)
