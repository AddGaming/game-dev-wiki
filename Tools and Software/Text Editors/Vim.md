---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-20
tags: 
- software
- text-editor
- missing
---


# Vim

[[Vim]] is a command line [[Text Editor|text editor]].



_____

## Sources

- [Vim - Webpage](https://www.vim.org/)
- [Neovim - Webpage](https://neovim.io/)