---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-20
tags: 
- software
- text-editor
- missing
---

# Text Editor

*definition*


## List of Text Editors

```dataview
table
from #text-editor  
where !contains(file.path, this.file.path)
```

_____

## Sources
