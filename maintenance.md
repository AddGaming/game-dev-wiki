# Maintenance

## Missing

```dataview
list
from #missing 
sort length(file.inlinks)
```

## Needs Review


```dataview
list
from ""
where row.authors and length(row.authors) < 2 and !contains(row.tags, "missing")
sort length(file.inlinks)
```

## Check if outdated

```dataview
table
date(row.last-edit) as "Last Edit"
from ""
where row.last-edit and row.last-edit <= date(today) - dur(100 day)
sort date(row.last-edit) asc 
```