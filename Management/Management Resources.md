---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-11-20
tags:
- Management
- meta
---

# Management Resources

## YouTube Channels

- [Allen Holub](https://www.youtube.com/@AllenHolub)

## Books

- [The Mythical Man-Month. Essays on Software Engineering - Frederick Brooks](https://www.amazon.de/-/en/Frederick-Brooks/dp/0201835959)
