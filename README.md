**An open wiki where game developers share information about the development of games.**

# [Start reading now](https://addgaming.gitlab.io/game-dev-wiki/)

If you want, you can clone this repository via 

```
git clone https://gitlab.com/AddGaming/game-dev-wiki.git
```

to have it available offline.

If you want to contribute, open a new issue [here](https://gitlab.com/AddGaming/game-dev-wiki/-/issues/new)
and insert your page with the corresponding template.
