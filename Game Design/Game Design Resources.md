---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-11-20
tags:
- design
- meta
- missing
---

# Game Design Resources

## Books

## YouTube Channels

- [Golden Owl](https://www.youtube.com/@GoldenOwlYT)
- [Timothy Cain](https://www.youtube.com/@CainOnGames)
- [Design Doc](https://www.youtube.com/@DesignDoc)
- [Game Maker's Toolkit](https://www.youtube.com/@GMTK)
- [Masahiro Sakurai on Creating Games](https://www.youtube.com/@sora_sakurai_en)
