---
last-edit: 2023-10-20
---


# The Game Dev Wiki

This is an attempt to centralize and organize information about game development.

The whole project is also downloadable for offline consumption under [this link](https://gitlab.com/api/v4/projects/51389976/repository/archive.zip).
You can also clone the project using git: 
- HTTPS: `git clone https://gitlab.com/AddGaming/game-dev-wiki.git`
- SSH: `git clone git@gitlab.com:AddGaming/game-dev-wiki.git`

> currently tracking more than **10** external links and **80** internal sites

If you open the offline version with the editor [Obsidian](https://obsidian.md/download), you will get more features and search functionality.
For more information how you can use this project with Obsidian, refer to the [[GDW and Obsidian|relevant page]] 

Each page in this wiki should give an overview of a specific topic with further curated resources in the `Sources` section, if you want to dive deeper into a particular topic.

## Start reading now

- [[Programming Language Overview.canvas|Programming Language Overview]]
- [[Game Breakdowns]]
- [[Game Engine| Game Engines]]
- [[Game Library| Game Libraries]]

## Credits

```dataview
TABLE WITHOUT ID 
"[" + author[0] + "](" + author[1] + ")" AS Authors,
length(rows.file.link) AS Pages
FROM "" 
WHERE authors FLATTEN authors AS author 
GROUP BY author SORT length(rows.file.link) DESC
```
