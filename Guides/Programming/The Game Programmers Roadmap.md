---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-11-12
tags: 
- guide
- programming
---

> If you are from an adjacent discipline and just want to learn a bit of coding to make your live easier, check out [[Programming for Non-Programmers]]
> If you are looking for information on how to program your first game, look here: [[Programming your first Game]]

# The Game Programmers Roadmap

This guide should outline the different topics one should learn in order to become a good game programmer.
Acquiring the knowledge in all the topics mentioned here is a multi year endeavor.
While it may take long, this still only covers the basics of each area, so you will most likely spend even more time in some specific areas to become an expert in.
The sources are at the end of the page.

## Programming Languages

### Your first language

Before engaging with different game related concepts, you first need to learn the basics of programming.
The most fundamental of those is the ability to express one self clearly and precise.
This is what programming languages are for.

You will learn multiple languages in your career with different [[programming paradigm | programming paradigms]] at their heart.
It has proven that it doesn't matter with what paradigm you start.
So your first language should be be: 

- simple to set up
- easy to read
- supported enough

Languages like [[Python]], [[C Language|C]], [[Ocaml]] and [[Kotlin]] are therefore good languages to start with.
But if you would like to start with a different language, focus on readability first when selecting one.

### Leaning a language

The best way to learn a language is to solve problems with it on different scales.
If you are starting out and familiarizing your self with the syntax, then sites like [Code Wars](https://www.codewars.com/), [Leet Code](https://leetcode.com/) and [Advent of Code](https://adventofcode.com/) help you.
They each provide small contained problems that can be solved within 10 minutes or multiple days.

But this will only bring you so far.
To learn more about your language, you will need to start a bigger project in them.
What classifies as a bigger problem changes from language to language.
But since you are trying to become a games programmer, try to make games within them.
Try to avoid [[Game Engine|game engines]] for this and favor [[Game Library|game libraries]].
The goal is to learn the language, not an engine.

### How many Languages do I need?

You don't need to know everything about every programming language.
Languages are tools and therefore there is always the trade-off between familiarity and how well the tool fits the problem.

It is therefore recommended to have one language each that fulfills the following needs:

- one [[Low-Level programming language]] for performance critical applications
- one [[High-Level programming language]] for scripting high level procedures
- one language for making web-sites (no, [[HTML]] and [[CSS]] don't count)
- one language for making console games ([[C Language|C]]/[[C++]]/[[CSharp|C#]])

## Software Design

Now that you learned to speak, you will need to learn how to structure speech.
Software design concerns it self on how to structure programs in a way, to keep them flexible and maintainable.

This is the point where you want to brush up on [[programming paradigm|programming paradigms]] and [[Programming Principle|programming principles]].
Try to understand how they came to be and how they changed over time.
Look back on the code that you have written and judge it by the new things you have learned.
Then try to improve the code.

While learning about software design takes you an evening, if you only learn the high level concepts,
understanding them and being able to apply them in code will probably take the rest of you life.

## Management

You will need to lean how to manage your tasks.

How do you create a work environment in which you can be your most productive self?
How to approach software projects in a way that ensures quality?
How do you effectively communicate with you team?

You should learn about basic notions of management, to answer the questions above.

While learning management stay vigilant for false information from certificate mills and consulting firms.
Both have proven to forgo any promises they make for the sake of profit.
Make many experiments your self to validate the information you're receiving.
Measure and compare, not only in the way of [Taylorism](https://en.wikipedia.org/wiki/Scientific_management), but also with an eye on the well being of your self and your co-workers.

## Optimization

You will need to write not only good looking, but also fast code as a games programmer.
Every millisecond saved is worth gold, since it enables you to have a better framerate, better response times and with that a better user experience.
Some optimizations are even so good, that it might lift constraints of the artists on your team. 

### CPU

While gamer often focus on the graphic cards first, the CPU is the first bottle neck that should be understood.
Understanding how [threads](https://en.wikipedia.org/wiki/Thread_(computing)) work on the operating system you are targeting 
and how you can take advantage of [SIMD](https://en.wikipedia.org/wiki/Single_instruction,_multiple_data) increases you ability to process data on the central part that is running your game.
A big part in all optimizations, not only CPU, is caching.
Knowing when to cache, and how to align caches for best traversal is extremely important. 

While graphic cards have become widely spread, not all graphic cards offer the same capabilities and power.
Hence it is possible that you'll find yourself stuck implementing algorithms for graphics on your CPU.
Knowing how to implement them effectively, will be important in such situations.

### GPU

Most GPUs are designed for many small parallel computations.
This changes the way you have to structure your data and think about performance.

Learning how to speak with graphics cards directly with APIs like [CUDA](https://developer.nvidia.com/cuda-zone#:~:text=CUDA%C2%AE%20is%20a%20parallel,harnessing%20the%20power%20of%20GPUs.) 
will increase your ability to manipulate the data-flow between your CPU and GPU.

Learning how to program shaders, even if you will not end up designing them, is also important, 
because you'll be most likely responsible for optimizing their performance.
Websites like [shadertoy](https://www.shadertoy.com/) allow you to quickly get accustomed to writing them.

## Testing

Testing is extremely vital to any software project that should run longer than a week.
Tests ensure the correctness of your programs output to a desired one.
This means you can test not only, that you are deducting the correct amount of HP from an enemy, 
but also things like minimum contrast in a scene or react-ability of attacks from enemies.

Finding ways of testing such things easily with the toolset of your choice, be it a [[Game Engine|game engine]] or something home brewed, 
is important to maintain the speed of development and keep the software bug free.
If you have good tests in place, you will also be able to find any bug within a few minutes.

## Engines & Libraries

Same as languages are but tools in your toolbox, same goes for [[Game Engine| game engines]] and [[Game Library|libraries]].
While you can certainly make games easily with libraries with just the knowledge of the programming language,
you will not be able to do the same with engines.
Libraries will come in when the features of the engine end or if you want increase flexibility by writing the glue your self.

Hence, knowing at least one popular [[Game Engine| game engine]] well and a [[Game Library|library]] for all components of a game (Networking, Graphics, Audio) is important.

The process of learning game engines can be accelerated if you have written games without using one.
This is, because you already know *what* is needed and you are only learning *"how"* it's done inside the engine. 

_____

## Sources

### Programming Languages

- [Structure and Interpretation of Computer Programs - Harold Abelson and Gerald Jay Sussman with Julie Sussman](https://web.mit.edu/6.001/6.037/sicp.pdf)
- [Introduction to Functional Programming and the Structure of Programming Languages using OCaml - Gert Smolka](https://github.com/uds-psl/Prog)
- [Functional vs Array Programming - code_report](https://youtu.be/UogkQ67d0nY?si=XmbzDBHxwvcy4Y_w)
- [From C -> C++ -> Rust - code_report](https://youtu.be/wGCWlI4A5z4?si=FlgwMkJOKmkSU7iI)
- [Systems that run forever self-heal and scale - Joe Armstrong](https://youtu.be/cNICGEwmXLU?si=vI70kzFYrGGzKynM)
- [Monad I Love You Now Get Out Of My Type System - Gjeta Gjyshinca](https://youtu.be/2PxsyWqZ5dI?si=O3bhdbwIj4Z7eBo1)

### Software Design

- [Messaging Pattern - Mike Wood](https://youtu.be/8B83elj_Z5o?si=CxdeZ0ooB7krZpag)
- [Design by Coding - Allen Holub](https://youtu.be/d5Y1B1cmaGQ?si=3G9ZPg0mKgxD6jMX)
- [Agile Architecture Part 1 - Allen Holub](https://youtu.be/0kRCFVGpX7k?si=GC4kgImXCb-fvS9Z)
- [Agile Architecture Part 2 - Allen Holub](https://youtu.be/CYCNRCrX1zE?si=PUXO1paVdP_4Zajt)
- [Seminar with Alan Kay on Object Oriented Programming](https://youtu.be/QjJaFG63Hlo?si=WN4Bh50TZ7oHx09q)
- [Reactive Systems - Dave Farley](https://youtu.be/tKRa0O7aepo?si=3go-eL3Z9cQ8gInn)
- [Gilding the Rose: Refactoring-Driven Development - Kevlin Henney](https://youtu.be/kTcDBYCpj7Q?si=ID-D1R6Vwmx9AEbW)
- [Five Lines of Codes - Christian Clausen](https://youtu.be/APdaacGmDew?si=RGIbu5wyqEAkz4Pr)
- [I See What You Mean - Peter Alvaro](https://youtu.be/R2Aa4PivG0g?si=vj6HZGbPAeCCZZwT)
- [C4 Models as Code - Simon Brown](https://youtu.be/f7i2wxQVffk?si=dGUTpSEBkqNdrmjh)
- [Core Design Principles for Software Developers - Venkat Subramaniam](https://youtu.be/llGgO74uXMI?si=mzX42yOIWbRzPdjG)
- [The SOLID Design Principles Deconstructed - Kevlin Henney](https://youtu.be/tMW08JkFrBA?si=BGyp3mYjQRqO4TEe)
- [You can't be agile when you're fighting your code - Allen Holub](https://youtu.be/aDLxvyn3Abc?si=SOyZN2EepTv3c54w)

### Management

- [The Only Unbreakable Law - Molly Rocket](https://youtu.be/5IUj1EZwpJY?si=gTfZ-nA-RWnkTJPa)
- [One Rule to Rule Them All - Pragmatic Dave Thomas](https://youtu.be/QvK3Pxmwcyc?si=k-kUuBhV3KoAfoWk)
- [Economies of Speed - Dave Farley](https://youtu.be/L-W2HbSOaWo?si=4m7JrHz-sHbGWaw7)
- [Git Flow Is A Bad Idea - Dave Farley](https://youtu.be/_w6TwnLCFwA?si=FYeRio4qB8a17Pw6)
- [Accelerate - Nicole Forsgen, Jez Humble & Gene Kim](https://www.amazon.de/-/en/Nicole-Forsgren-PhD-ebook/dp/B07B9F83WM)

### Optimization

- ["Clean" Code, Horrible Performance - Molly Rocket](https://youtu.be/tD5NrevFtbU?si=GIGjwscWLt84CKiK)
- [Perfomance Matters - Emery Berger](https://youtu.be/r-TLSBdHe1A?si=d6xsqOyMjQDQapZz)
- [Adding Nested Loops Makes this Algorithm 100x FASTER? - DepthBuffer](https://youtu.be/QGYvbsHDPxo?si=q97qEx9OcUSR1GGY)
- [Shadertoy - Website](https://www.shadertoy.com/)
- [CUDA - Website](https://developer.nvidia.com/cuda-zone#:~:text=CUDA%C2%AE%20is%20a%20parallel,harnessing%20the%20power%20of%20GPUs.)

### Testing

- [Unit Testing is The BARE MINIMUM - Dave Farley](https://youtu.be/h-4i5N89TUI?si=Tjh9l1c9Is-Kp1KQ)
- [Test Smalls and Fragrances - Kevlin Henney](https://youtu.be/wCx_6kOo99M?si=RWrVgalqVFnGlgOW)
- [Structure and Interpretation of Test Cases - Kevlin Henney](https://youtu.be/MWsk1h8pv2Q?si=kTSytkrk4abxVhS0)
- [Finding bugs without running or even looking at code - Jay Parlar](https://youtu.be/FvNRlE4E9QQ?si=tSpHiPR1miK1bCjI)

### Engines & Libraries

- [[Game Engine]]
- [[Game Library]]

### General

- [Solving the Right Problems for Engine Programmers - Mike Acton‌](https://youtu.be/4B00hV3wmMY?si=2mOpD1yxNIxga9RH)
- [[General Programming Resources]]