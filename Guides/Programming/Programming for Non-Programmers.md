---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2024-02-17
tags: 
- guide
- programming
- missing
---

> If you want to learn how to program to become a game programmer, check out [[The Game Programmers Roadmap]] instead
> If you are looking for information on how to program your first game, look here: [[Programming your first Game]]

# Programming for Non-Programmers

Programming for disciplines that are mainly concerned with the creation of game assets doesn't require nearly as much knowledge about the subject,
as gameplay programming needs.
It is possible to learn everything that one needs to know in one afternoon.

## Programming Languages

While a programmer needs to know many different languages for different use cases, an artist only needs two.
[[Python]] and [[Bash]]/[[Batch Language|Batch]] are the two/three languages an artists needs.
All three of them are scripting languages and are compatible small in scope and features.

[[Python]] enjoys a high level of popularity as a scripting languages which lead to many software products implementing the option to script them in [[Python]].
[[Bash]] and [[Batch Language|Batch]] on the other hand get interpreted by consoles and terminals.

## Software

### For Writing Code

While you can write code with any text editor, [[VSCode]] has proven to be a good mix of extensibility for programmers and user friendliness for non technical people.

### For Managing Code

[[Git]] is the most widely used [[Version Control System]] and is supported by a wide range of online vendors.

## Knowledge

You don't need to know much to reap the benefits of scripting.
Here is a quick check list which can be worked through in an evening:

1. What are variables?
2. What are functions?
3. What is a for loop?
4. How do I interact with a file?
5. What is the command line?
6. How do I call a program from the command line?
7. How do I accept input from the command line

With this, you should be able to automate much of your daily tasks.

## Guidelines 

There are some guidelines which you should follow, to keep programming easy even for larger pipelines:

### Pure Functions

No function should alter its inputs

**BAD:**
```py
def my_fun(arg1: int):
  arg1 += 1

x = 1
my_fun(x)
# x now is 2
```

**GOOD:**
```py
def my_fun(arg1: int) -> int:
  return arg1 + 1

x = 1
x = my_fun(x)
# x now is 2
```

### Clear Names

Naming is a genuinely hard problem.
But in many cases an honest attempt is way better for understandability of a third party than just using single letter variables

**BAD:**
```py
def fn(e, b, x):
  a = S()
  b = a ^ e / b
  return x * 3
```

**GOOD:**
```py
def fn(exponent, base, text):
  foundation = Foundation()
  new_base = foundation ^ exponent / base
  return text * 3
```

_____

## Sources

- [VSCode download]()
- [Git download]()
- [Python download]()
- [[Python|Python entry in this wiki]]