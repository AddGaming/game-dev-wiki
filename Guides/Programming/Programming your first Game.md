---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2024-02-17
tags: 
- guide
- programming
---

> If you want to learn how to program to become a professional game programmer, check out [[The Game Programmers Roadmap]] instead
> If you are from an adjacent discipline and just want to learn a bit of coding to make your live easier, check out [[Programming for Non-Programmers]]

# Programming your first Game

This is an outline of the required knowledge to master the programming aspect of making your first game.
The sources are at the end of the page.

## Choosing your Goal

Before you can develop a game, you need to choose a type of game to make.
If you don't know yet what to do, you can always [choose a topic at random](https://letsmakeagame.net/game-idea-generator/)

### Genres to Avoid

If you wanted to make a game of the following genres, don't feel discouraged.
There are alternative first games listed which learnings will enable you to release your dream game eventually. 

#### MMO

*Massively multiplayer online* games (*MMO*s) are bad first projects.
The required knowledge in the most difficult areas of programming is extremely high.
Namely distributed computing, data synchronization, latency, cyber security and reliability.
That said, the genre acronym seldom ends with `O`.
So why not make a game fitting the other genre first?

If you want to make an **MMO*RPG*** (like World of Warcraft), make a smaller **RPG** first.
If you want to make an **MMO*FPS*** (like Destiny), make a smaller **FPS** first.

#### MOBA

*Multiplayer Online Battle Arena*s (*MOBA*s) are also an incredibly hard genre of games to make,
for the same reason MMOs are hard.
Hence, the same advice applies here.
Go and make a related game with smaller scope first.

If you want to make an MOBA like **League of Legends**, make a smaller **Top Down Brawler** first.
If you want to make an MOBA like **SMITE**, make a smaller **3rd Person Brawler**.

#### Fighting Games

Fighting games like Street fighter, Skull Girls, ... are too hard for a beginner, 
since networking and performance in those games is extremely pivotal.
These are additional things which will it extremely hard up to impossible for a beginner to make a successful fighting game.

If you want to make fighting games eventually, consider making your first game a simpler side-scroller or jump-and-run.

## Choosing a Framework

Now that you have a goal, you can pic a Framework.

If you have no prior experience, making a game from scratch is not advised.
There are a plethora of frameworks for developing games, which take care of many different aspects of game development.
The most popular are [[Game Engine|game engines]] like [[Unreal Engine]], [[Unity]] or [[Godot]].
[[Unreal Engine]] and [[Unity]] are not recommended for complete beginners, since the amount of features which makes development for experienced developers easy,
is an unnecessary first hurdle in making a game.

Each framework will have an associated programming language.
Your first language should be be: 

- simple to set up
- easy to read
- supported enough

Languages like [[Python]], [[C Language|C]], and [[Kotlin]] and [[Godot]]s GDScript are therefore good languages to start with.
But if you would like to start with a different language, focus on readability first when selecting one.
Here are example frameworks for the mentioned languages:

| Language | Frameworks             |
| -------- | ---------------------- |
| Python   | PyGame, Pyglet, Ren'Py |
| C        | GB Studio, RayLib      | 
| GDScript | Godot                  |
| Kotlin   | KorGE                  |

If you use another language, take a look at the [EnginesDatabase](https://enginesdatabase.com/).

## Making the Game

By now you should have a simple description of your goal with which google or other people can help you on your journey.
It should look something like this:
```
I'm making a Zelda like with Kotlin and KorGE
```
or 
```
I'm making a Factorio clone with Python & Pyglet
```

Now the only thing that is left to do is to make the game.
The easiest path will be to look for online tutorials and documentation of your Framework.
Most frameworks come with examples and explainers which should help you to get a grip on them.

Good luck and have fun!

_____

## Sources

- [Game Idea Generator](https://letsmakeagame.net/game-idea-generator/)
- [raylib - Homepage](https://www.raylib.com/)
- [GB Studio - Homepage](https://www.gbstudio.dev/)
- [PyGame - Homepage](https://www.pygame.org/docs/)
- [Clear Code - PyGame tutorial YouTube channel](https://www.youtube.com/@ClearCode)
- [pyglet - Homepage](https://pyglet.org/)
- [Ren'Py - Homepage](https://www.renpy.org/)
- [Godot - Homepage](https://godotengine.org/)
- [KorGE - Homepage](https://korge.org/)
