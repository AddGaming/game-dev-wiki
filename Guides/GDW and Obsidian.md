---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-23
tags: 
- guide
- obsidian
---

# GDW and Obsidian

Here is a guide for using this wiki offline with the tool [[Obsidian]].

First, you'll need to [download Obsidian](https://obsidian.md/download).
After that, you will need to download the wiki to your PC.
There are two ways of doing this:

1. Use git: `git clone https://gitlab.com/AddGaming/game-dev-wiki.git`.
2. Use the [download link](https://gitlab.com/api/v4/projects/51389976/repository/archive.zip) and extract the downloaded `zip` folder.

After you downloaded both [[Obsidian]] and the Game-Dev-Wiki (GDW), start obsidian.

![[obsidian vault select.png]]

Now press `Open` next to `Open folder as vault` and select the folder, where you saved the GDW.
Now you should be greeted with the `No file is open` screen.

## Opening a File 

There are 3 ways, you will be navigating the in [[Obsidian]] the most often.

### The Side Panel

The side panel can be opened and closed by clicking on the icon in the top left corner.
The icons are (from left to right): *Side Panel Toggle*, *Folder Structure*, *Search*, *Book Marks*.

Of interest to us is the folder structure.
Here you have a list of folders and files that compromise the GDW.
You can open a specific file by clicking on it.

### With Fuzzy Finding

To open the file search press `Ctrl + O`.
This will launch a selection window in which you can now type the name of the file you want to open.

You can navigate the selection with the arrow keys.
Press `Enter` to confirm your selection.

Why not try it with `index`? This will lead you to the *"Home Page"* of the web-site.

### With Search

While the fuzzy finder is great, this requires you to know the file name.
What if you want to search for a specific topic or don't know the exact name?
This is what the `Search` is for.

To open the search window, press `Ctrl + Shift + F`.
The now open dialog will inform you that this window can search for paths, file names, tags, and file content.

You can navigate the results with the arrow keys. 
Confirm your selection with `Enter`.

#### Content Search

Let's navigate to the `index` page again, but this time by content.
Open the search, type `if you open the offline version`.
There should be plenty of results, all trying to match what you have typed.
But we know that the above quoted section exists verbatim in the `index`.

To limit the results, put the search into quotation marks: `"if you open the offline version"` or `'if you open the offline version'`.
Now there should only be two results. One this file and the other the desired `index`
Select it with the arrow keys and press `Enter`.

#### Tag Search

Let's try getting a selection on a specific topic.
For that, open the search window and type `tag:` followed by the tag you are interested in.
All tags start with a `#` by convention.

As an example: 
- `tag:#programming` will show you all programming related pages.
- `tag:#text-editor` will show you all entries for text editors.
- and `tag:#game-engine tag:#Windows` will show you all game engines that are available on windows
 
## The Graph

Like in the web site, you also have a graph view of the project here.
To open the graph, press `Ctrl + G`.

But what is the advantage over the web representation?
Well, you can filter the graph, like you can limit the search. 

### Filtering

If they are not already open, open the settings by pressing the cog on the top right site.
Then expand the section `Filters`.
There you can put in the same search queries as you did before in the search window.

Try limiting the graph to only programming languages by entering `tag:#language`.

But now we are missing the middle nodes, that connect the languages and depict the relations between them.
Remove the filter again and go down to the next section: `Groups`.

### Grouping

Press the button `New Group`.
Now [[Obsidian]] will ask you to enter *"a query"*. 
For now, lets insert the same thing from above: `tag:#language`.

We can now see, that the nodes fitting into this group, have a different color from the rest.
This method preserves the relationships between nodes better than filtering, but sacrifices some clarity, since there are many more nodes on the screen.

Of course you can combine both methods.

## Plugins

Within some pages, you will encounter the sections looking like this:

```
dataview
list from "somewhere"
```

Those are dynamically generated lists and tables to reduce the maintenance burden on the maintainers.
To render them correctly, you will need a plugin.

Open the settings by pressing `Ctrl + ,` and go to `Options > Community plugins`.
Now press the `Browse` button and search for `dataview` and install the plugin from Michal Brenan.
The plugin should now be visible inside your `Installed plugins` list.
If it isn't already activated, activate it now.

The tables should now all generate correctly.
Here is a small list to test it:

```dataview
list
where contains(file.path, this.file.path)
```

## Conclusion

You should now have a good grip on navigation in [[Obsidian]].
But the software can do much more. 
Check out the [official documentation](https://help.obsidian.md/Home) to learn more about the features.

In case you want to contribute to the project you might want to check out the page `maintanace` which is not included in the web version for obvious reasons.
There are a list of tables guiding you to places that can need improvement.

_____

## Sources

- [Obsidian - Website](https://obsidian.md)
- [Obsidian Documentation](https://help.obsidian.md/Home)
- [Game-Dev-Wiki - Repository](https://gitlab.com/AddGaming/game-dev-wiki)
- [Dataview - Website](https://blacksmithgu.github.io/obsidian-dataview/)

