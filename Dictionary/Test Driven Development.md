---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-11-11
tags: 
- meta
- testing-approach
- testing
alias: TDD
---

# Test Driven Development

[[Test Driven Development]] defines a loop for development of software which closely aligns with other engineering loops like [OODA](https://en.wikipedia.org/wiki/OODA_loop).
The loop is: 

1. **red** - write one or multiple tests testing the new feature that you want to implement.
2. **green** - write the functionality and fulfill all the requirements posed by the tests you wrote prior.
3. **blue** - in this stage you should refactor the things you have written, now with the security that the tests give you.

Depending on the source, the loops also gets called *Red, Green, Refactor*.


## Modifications to the Loop

An alteration of the loop is *Refactor, Red, Green*.
The *refactoring* step is completed if the new functionality can be implemented in 5 lines or less.

This alteration got proposed since many developers not used to regular refactors of code often omit the last step of the classic *RGB* loop.
This leads to unmaintainable code bases in the long run, since all additions are made without considerations of the whole software.

By pulling this often committed step to the beginning of the process with a strict criteria when it is finished,
the proponents encourage thinking about the structure of the whole system more often and more in depth.

## How to write tests for something you know nothing about

This is one straw-man counter argument that opponents of TDD (like [ThePrimeagen](https://youtu.be/IInciWyU74U?si=OxILhhwtkacRt95v) or [Jonathan Blow](https://youtu.be/21JlBOxgGwY?si=v9-sI3SkJbpeKvlb)) often bring up.
How can I tests something when I don't know what that something will be?
The answer is that you don't.

The testing first approach is meant to get you thinking about what you want.
If you can't formulate that clearly in a test, you don't know what you want yet.

There are two ways of fixing this state:

1. Go and build a proof of concept.
2. Observe the problem domain. 

In some cases both can work, but in most cases, only building a proof of concept 
will surface all the edge cases that you will need to think about when implementing the real deal.

The opponents say that this is hindering your progress and they are right.
This is a break on your development speed with the goal of ensuring a higher level of quality.
Reducing the decisions in the code base that were made by gut instinct and increasing the intentional decisions by the programmer is a good thing.


## When to skip TDD

Now you have to decide what the half life of your game will be.
If the expected half life of your software is smaller than the time testing takes, it can be wise to no choose TDD.
This is usually in domains that venture in markets where time to market is more important the the over all quality of the product.
In markets where the quality or long lifespans are more important; the longer upfront development time will be worth it, 
since the increased amount of tests help you to keep the system malleable for the long term. 

_____

## Sources

- [Kevlin Henney - Gilding the Rose: Refactoring-Driven Development](https://www.youtube.com/watch?v=kTcDBYCpj7Q)
- [Ian Cooper - TDD Revisited](https://www.youtube.com/watch?v=IN9lftH0cJc)
