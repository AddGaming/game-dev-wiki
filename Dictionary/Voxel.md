---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2024-01-10
tags: 
- meta
- missing
---

# Voxel

_____

## Sources

- [Davix Marley - Greedy Meshing Voxels Fast - Optimism in Design](https://www.youtube.com/watch?v=4xs66m1Of4A)
