---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-26
tags: 
- meta
- game-break-down
---

# Game Breakdowns

Game breakdowns are detailed description of games and their development.
If you want to learn more on how your favorite game was made or to make a game just like it, this is the perfect place to start.

```dataview
List FROM #game-break-down
where file.name != this.file.name
```
