---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-19
tags: 
- game-engine
- programming
---

# Game Engine

A game engine is a holistic framework which provides all capabilities required to develop a full game.
A game engine is a wholesale package in contrast to [[Game Library]]s.

Game engines can be categorized by the features that they provide the developer and the platforms they can ship games to.

## Table of Game Engines

```dataview
table
choice(contains(row.tags, "Windows"), "✅", "❌") as Windows, 
choice(contains(row.tags, "MacOS"), "✅", "❌") as MacOs, 
choice(contains(row.tags, "Linux"), "✅", "❌") as Linux,
choice(contains(row.tags, "2D"), "✅", "❌") as "2D Support", 
choice(contains(row.tags, "3D"), "✅", "❌") as "3D Support", 
join(row.languages, ", ") as "Supported Langages",
join(row.features, ", ") as Features,
join(row.can-ship, ", ") as "Can ship to"
from #game-engine 
where !contains(file.path, this.file.path)
sort length(row.features) + length(row.tags) + length(row.can-ship) desc 
```

_____

## Sources

- [Wikipedia - Game engine](https://en.wikipedia.org/wiki/Game_engine)
