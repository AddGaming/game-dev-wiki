---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-11-20
tags: 
- game-engine
- bevy
- programming
- Windows
- MacOS
- Linux
- Web
- 3D
- missing
can-ship:
- PC
- Mobile
- Web
features:
- ECS at core
languages:
- Rust
---

# Bevy

[[Bevy]] is a modular [[Rust]] [[Game Engine]].

_____

## Sources

- [Unofficial Bevy Cheat Book](https://bevy-cheatbook.github.io/introduction.html)
- [ZymartuGames - YouTube](https://www.youtube.com/@ZymartuGames)
