---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-11-20
tags: 
- game-engine
- unity-engine
- programming
- Windows
- MacOS
- Linux
- 3D
- 2D
- missing
can-ship:
- PC
- Mobile
- Playstation
- XBox
features:
- GUI
- Visual-Scripting
languages:
- C#
---

# Unity

The [[Unity]] engine is a [[Game Engine|engine]] and direct competitor of [[Unreal Engine]].

## Installation


_____

## Sources

- [Brendan Dickinson](https://www.youtube.com/@BrendanDickinsonUnity)
- 

