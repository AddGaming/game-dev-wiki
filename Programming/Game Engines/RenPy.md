---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-20
tags: 
- game-engine
- programming
- Windows
- MacOS
- Linux
- 2D
- missing
can-ship:
- PC
- Mobile
- Web
features:
- "easy scripting language"
languages:
- RenPy-Script
---

# RenPy

The [[RenPy]] is a [[Python]] based [[Game Engine|game engine]] for creating [[Visual Novel|visual novels]].
The code is open source [on their GitHub](https://github.com/renpy/renpy).

## Installation

You can download the current version of the engine [from their website](https://www.renpy.org/).


_____

## Sources

- [RenPy - Website](https://www.renpy.org/)
- [RenPy - GitHub](https://github.com/renpy/renpy)
- [Wikipedia - Ren'Py](https://en.wikipedia.org/wiki/Ren%27Py)
