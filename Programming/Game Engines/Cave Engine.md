---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-23
tags: 
- game-engine
- programming
- Windows
- 3D
- missing
can-ship:
- Windows
features:
- GUI
languages:
- Python
---

# Cave Engine

The [[Cave Engine]] is a 3D [[Game Engine|engine]] that uses [[Python]] as its scripting language.

## Installation

*content*

_____

## Sources

- [Cave Engine - Itch.io](https://unidaystudio.itch.io/cave-engine)
- [Cave Engine - Documentation](https://unidaystudio.github.io/CaveEngine-Docs/)
