---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-20
tags: 
- game-engine
- unreal-engine
- programming
- Windows
- MacOS
- Linux
- 3D
- missing
can-ship:
- PC
- Mobile
- Playstation
- XBox
features:
- GUI
- Visual-Scripting
languages:
- C++
- Bluescript
---

# Unreal Engine

The [[Unreal Engine]] is the leading [[Game Engine|engine]] when it comes to creating 3D game worlds.

## Installation

Windows and MacOS user download the engine through the *Epic Games Launcher*.
Linux user are referred to the [quick start guide by Epic](https://docs.unrealengine.com/4.27/en-US/SharingAndReleasing/Linux/BeginnerLinuxDeveloper/SettingUpAnUnrealWorkflow/).

_____

## Sources

- [Unreal Engine - Website](https://www.unrealengine.com/de)
- [Unreal Learning Platform - Website](https://dev.epicgames.com/community/unreal-engine/learning)
- [YouTube - Alex Forsythe](https://www.youtube.com/@AlexForsythe)
- [YouTube - Lively Geek](https://www.youtube.com/@LivelyGeekGames)
- [YouTube - Virtus Learning Hub](https://www.youtube.com/@VirtusEdu)
