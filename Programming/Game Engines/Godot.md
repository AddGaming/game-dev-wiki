---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-11-20
tags: 
- game-engine
- godot-engine
- programming
- Windows
- Linux
- 3D
- 2D
- missing
can-ship:
- PC
features:
- GUI
languages:
- C#
- Rust
---

# Godot

The [[Godot]] engine is a [[Game Engine|engine]] and direct competitor of [[Unity]].

## Installation


_____

## Sources

- [YoutTube - HeartBeat](https://www.youtube.com/@uheartbeast)
