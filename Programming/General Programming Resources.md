---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-11-20
tags:
- programming
- meta
- missing
---


# General Programming Resources

## YouTube Channels

- [ACCU Conference](https://www.youtube.com/@ACCUConf)
- [Brian Will](https://www.youtube.com/@briantwill)
- [code_report](https://www.youtube.com/@code_report)
- [Codemy.com](https://www.youtube.com/@Codemycom)
- [Context Free](https://www.youtube.com/@contextfree)
- [Continuous Delivery](https://www.youtube.com/@ContinuousDelivery)
- [Devoxx Conference](https://www.youtube.com/@DevoxxForever)
- [DevWeek Conference](https://www.youtube.com/@devweekevents5952)
- [DigiPen Game Engine Architecture Club](https://www.youtube.com/@GameEngineArchitects)
- [Fireship](https://www.youtube.com/@Fireship)
- [FreeCodeCamp](https://www.youtube.com/@freecodecamp)
- [GOTO Conference](https://www.youtube.com/@GOTO-)
- [Low Byte Productions](https://www.youtube.com/@LowByteProductions)
- [NDC Conference](https://www.youtube.com/@NDC)
- [NullPointer Exception](https://www.youtube.com/@NullPointerException)
- [polylog](https://www.youtube.com/@PolylogCS)
- [Strange Loop Conference](https://www.youtube.com/@StrangeLoopConf)
- [The Cherno](https://www.youtube.com/@TheCherno)
- [Handmade Cities Conference](https://vimeo.com/handmadecities)
- [HandmadeCon](https://www.youtube.com/playlist?list=PLEMXAbCVnmY6HE0bxUHeLLP5QbBE2zhJi)


## Books

- [97 things every programmer should know](https://github.com/97-things/97-things-every-programmer-should-know)
- [Building Microservices: Designing Fine-Grained Systems - Sam Newman](https://amzn.eu/d/jhc8dxg)
- [Monolith to Microservices: Evolutionary Patterns to Transform Your Monolith - Sam Newman](https://amzn.eu/d/b6Nwwxn)
