---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-25
tags: 
- programming
- game-library
- Windows
- missing
categories: 
- graphics 
- audio
- io
languages:
- Python
---


# Pyglet

[[Pyglet]] is a [[Game Library]] for [[Python]] which provides basic wrapper for 2D graphic displaying and sound.


_____

## Sources

- [Pyglet Website](https://pyglet.org/)
