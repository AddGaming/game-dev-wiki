---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-26
tags: 
- game-library
- programming
---

# Game Library

*content*

## Table of Game Libraries

```dataview
table
choice(contains(row.tags, "Windows"), "✅", "❌") as Windows, 
choice(contains(row.tags, "MacOS"), "✅", "❌") as MacOs, 
choice(contains(row.tags, "Linux"), "✅", "❌") as Linux,
row.category as Category,
join(row.languages, ", ") as "Supported Langages",
join(row.features, ", ") as Features
from #game-library 
where !contains(file.path, this.file.path)
sort length(row.features) + length(row.tags) + length(row.can-ship) desc 
```

_____

## Sources

