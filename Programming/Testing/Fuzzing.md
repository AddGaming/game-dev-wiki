---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-12-19
tags: 
- testing
- programming
- test-category
- nondeterministic
---

# Fuzzing | Fuzz Testing

Fuzzing are [[Testing#Nondeterministic Tests|nondeterministic]] [[Blackbox Testing|black box]] [[System Testing|system tests]] that evaluate a program against (pseudo) random input.

This means that fuzz tests will never (expect in very trivial cases) be able to check all possible options for a given program.
Hence, the programmer must decide on a reasonable amount of time the fuzzer should run each activation.
Also important is that the fuzzer reports detailed error messages, since the input is (pseudo) random which makes a specific fuzzer run unreproducible by design. 

## Structured vs Unstructured Input

When deciding how to fuzz, the programmer has decide on the degree that the input is constrained.

Let's say your game has on online forum that is accessible in game and outside the game from your website.
This will involve you having a open API which players send messages to and request data. Either through the website or through the game.
But this open endpoint is exposed and will possible receive any form of bytes of any length from malicious actors, or from different character sets.
Hence it makes sense to fuzz random bytes at the endpoint to check if content gets rejected if it doesn't make sense.

Another example: You are an offline game and you know that the physics library that you are using has ... *some quirks*.
You can use structured fuzzing of the player input in conjunction with observing the players position to determine if the player got stuck.
Simply throw a bunch of movement input at a random scene and look if the player suddenly stops moving even tho he still receives input.
You can expand this to also check for sporadic or sudden position shifts like, for example, teleportation over the map.

## Considerations

Fuzzing is an extremely ineffective way of testing your software.
Even tho it can be invaluable in some circumstances, it can never and should never replace other forms of testing.
Running servers that spend 90 to 99% of their time basically doing work to confirm already known functionality is in many cases a waste of energy and time.
The consideration if to fuzz or not hence should be tied to the likelihood of bugs impacting the experience of users or the possible severity of the bug.

Lets say you are working on a game at the scale of 10 million estimated units to be sold in the first month.
A bug that only occurs on very specific input that affects only 0.1 percent of your users would mean that 10 thousand users would be impacted by that bug.
This makes fuzzing probably worth it.
If you are expecting only 10 thousand units to be sold in the first month, this possible bug can impact 10 users.
Probably not worth fuzzing and instead hot fixing the issue when discovered by users.

The security aspect also happens on a gradient:
If you (please don't) embed a system to parse structured data like HTML inside your game you are probably *forced* to extensively fuzz the possible inputs,
since the likelihood of arbitrary code execution just went above 0.


_____

## Sources

- [AFLplusplus - Fuzz Everything, Everywhere, All at Once](https://youtu.be/yH_XPh_9Svo?si=q4MYvuLM181qmhyB)
