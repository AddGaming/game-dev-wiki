---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-12-19
tags: 
- testing
- programming
- testing-approach
---

# Whitebox Testing

[[Whitebox Testing]] is a [[Testing]] approach which **does** make assumptions about the underlying implementation.

This means that *whitebox tests* (opposite to [[Blackbox Testing|blackbox tests]]) are coupled to the underlying implementation.
It is generally discouraged to have persistent *whitebox tests* in your tests infrastructure,
since they are prone to breaking and slow down your development speed drastically.

