---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-12-19
tags: 
- testing
- programming
- test-category
- deterministic
aliases: 
- Smoke Testing
---

# Sanity Testing

[[Sanity Testing|Sanity tests]] or *smoke tests* are [[Blackbox Testing|black box]] tests that check basic assumptions of your code that *must* be true in all cases.
Sanity tests can be [[Unit Testing|unit]] or [[System Testing|sytem tests]] but must be [[Testing#Deterministic Tests|deterministic]].

An example of sanity tests would be that all costume implemented hash functions adhere to a certain set of criteria.
Or a `Hello World` program can be considered a *sanity test* for the development environment. 
[Checksums](https://en.wikipedia.org/wiki/Checksum) are often used to sanity test if an installation was successful.

Depending on the introspection capabilities of your language, setting up *sanity test* can be more or less time consuming.
Consider the hash example from before. In a language with good introspection like [[Python]], 
a test script could access the whole code and its [[Abstract Syntax Tree|AST]] to match function names to a given [[Regular Expression|regex]].
Doing the same in languages like [[C Language|C]] or [[C++]] would require writing a script which partially parses and compiles segments of the code,
which is much more work.

## Performance Testing

> ( *for performance tests as [[Acceptance Testing|acceptance tests]] look [[Acceptance Testing#Performance Testing|here]]* )

Performance tests are a form of [[Sanity Testing|sanity tests]] that check that the required time is not exceeded.
If you are developing a game with the target frame rate of 60 [[FPS]], 
then it makes sense to write a test to test that your game loop only takes a maximum of `1s / 60 = 0.016s = 16ms`.
Performance tests should never be used to diagnose the problem, but can always serve as a warning signal, that something is wrong.

## Pre-commit hooks

Another form of [[Sanity Testing|sanity tests]] are scripts that run before you commit your changes to your [[Version Control System|VCS]].
These hooks can bring functionality commonly found in [[Code compilation|compilers]] to languages with very weak or non existent [[Code compilation|compilers]].

In some trivial cases it also is possible to fix errors found by pre-commit tests automatically (e.g. formatting mistakes).

A pre-commit system can be set up via a simple [[Batch Language|Batch]] or [[Bash]] script, or via tools like [pre-commit](https://pre-commit.com/).

