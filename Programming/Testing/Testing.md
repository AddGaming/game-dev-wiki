---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2024-01-15
tags: 
- testing
- programming
---

# Testing

Software testing has the goal of assuring the quality of the build product.
Tests can be categorized by [[Testing#Test Categories|category]] and [[Testing#Testing Approaches|approach]].
Each category tries to test different part of the entire system while each approach describes how to go about testing that part.
That said, tests can only show the *existence* of bugs, and ***never*** their absence. 
Another axis on which tests can be ordered is whether they are [[Testing#Deterministic Tests|deterministic]] or [[Testing#Nondeterministic Tests|nondeterministic]].

Tests are a core requirement for complex systems and help keeping the system flexible and malleable.

## Test Categories

```dataview
list
from #test-category  
sort length(file.inlinks)
```

## Testing Approaches

- [[Test Driven Development]]
- [[Blackbox Testing]]
- [[Whitebox Testing]]
- [[Continuous Delivery#Continuous Testing|Continuous Testing]]
- [[Waterfall Development|Waterfall Testing]]

## Deterministic Tests

Deterministic tests are tests which outcome does not change when you repeat them.

### List of deterministic tests

```dataview
list
from #testing and #deterministic 
sort length(file.inlinks)
where !contains(file.path, this.file.path)
```

## Nondeterministic Tests

Nondeterministic tests are tests which outcome can change when you repeat them.

### List of nondeterministic tests


```dataview
list
from #testing and #nondeterministic 
sort length(file.inlinks)
where !contains(file.path, this.file.path)
```

## Related Guides

```dataview
list from #testing and #guide 
sort length(file.inlinks)
where !contains(file.path, this.file.path)
```

_____

## Sources

- [NATO science committee - Software Engineering Techniques](http://homepages.cs.ncl.ac.uk/brian.randell/NATO/nato1969.PDF)
- [Boot.dev - I MOCK YOUR MOCKS](https://blog.boot.dev/clean-code/writing-good-unit-tests-dont-mock-database-connections/)
- [Kevlin Henney - Test Smells and Fragrances](https://youtu.be/wCx_6kOo99M?si=9IMa75ax9HbqZpKT)
- [Robert Masella - Automated Testing of Gameplay Features in 'Sea of Thieves'](https://www.youtube.com/watch?v=X673tOi8pU8)
- [GDC - Behavior is Brittle: Testing Game AI Panel](https://www.youtube.com/watch?v=RO2CKsl2OmI)
- [Peter Buchardt - Design Tests and What to Expect from Them](https://www.youtube.com/watch?v=KSVzg6SEs48)
- [Jonas Gillberg - AI for Testing: The Development of Bots that Play Battlefield V](https://www.youtube.com/watch?v=s1JOSbUR6KE)
- [Jan van Valburg - Automated Testing and Profiling for Call of Duty](https://www.youtube.com/watch?v=8d0wzyiikXM)