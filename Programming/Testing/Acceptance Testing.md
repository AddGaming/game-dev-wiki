---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2024-01-10
tags: 
- testing
- programming
- test-category
- nondeterministic
- deterministic
---

# Acceptance Testing

[[Acceptance Testing|Acceptance tests]] are [[Testing#Nondeterministic Tests|nondeterministic]] [[Blackbox Testing|black box]] tests, 
which tests the *acceptance* of *the user* of the changes through the formation of *acceptance criteria*.

These can test underlying psychological guidelines for interface design or a predefined contract that has to uphold.

## Usability Testing

A subcategory of [[Acceptance Testing|acceptance testing]] is [[Acceptance Testing#Usability Testing|useability testing]] which aims to quantify the usability of the end product.

This can be done through simple UX guidelines like (4±2 aka [Miller's law](https://psychclassics.yorku.ca/Miller/)).

Another way is through contracts stated by the game designer.
Example: *"We want a game with a resource sink at the end. So the level progression should be linear except the last 5 levels where it increases exponentially."*
A test for this contract would test exactly that; that the increase is linear as a base and becomes exponential in the last 5 levels only.

### Performance Testing

> ( *for performance tests as [[Sanity Testing|sanity tests]] look [[Sanity Testing#Performance Testing|here]]* )

A special subcategory of [[Usability Testing|useability testing]] are [[Acceptance Testing#Performance Testing|performance tests]] which ensures a minimum bound on performance with the goal of maintaining a base level of reactiveness.

Performance tests should be used as [[Regression Testing|regression]]/[[Sanity Testing|sanity tests]], testing that the lower threshold is not crossed.
The can not help you diagnose the problem with your performance, but only show that a problem exists.

A basic example would be frame budgets.
If you want your game to run at 60 [[FPS]], then you aught to test that a update loop computes in a maximum of 16 milliseconds.

Splitting up performance tests further requires more through and experience with the type of game and hardware you are working with.
Lets assume that you want to naively split up the 16 ms to 10 ms rendering and 6 ms simulation.
You now have to know the hardware you are developing for more deeply and couple your tests to the hardware you are developing for.

- Do you have a dedicated graphics processing unit?
    If you have one, maybe the 10/6 split doesn't make sense since both are mostly independent.
- How and when are the data ready to be displayed on screen?
    What are the data dependencies? Can you make calculations for the background in 1 ms and the rendering takes 8 ms? Then you can use the time gained again to simulate the player in greater detail.
- How many cores of compute do you have and how do you distribute the data on them?

Having an idea of the performance you want to achieve is good. Setting it in stone when you don't know how the project will evolve is foolish.

### Accessibility Testing

A special subcategory of [[Usability Testing|useability testing]] are [[Acceptance Testing#Acceptance Testing|acceptance tests]] which tries to keep your application usable for a wide range of people.

This includes graphical tests that test for:

- A minimum contrast in the scene between important and background elements.
- Enough contrast through other measures than color (colorblindness).
- Font and object size in relation to screen size (for e.g mobile usage). 
- ...

Control based tests also can be formulated to test e.g: that the menus on a PC title can be navigated with a key board and not only a mouse.
