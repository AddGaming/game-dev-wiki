---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-12-19
tags: 
- testing
- test-category
- programming
- deterministic
---

# Unit Testing

A unit test is a small tests meant to test the correct working of a small unit of the whole system.
This is usually on the level of single functions which rank low in the overall abstraction hierarchy. 
Such tests can be either [[Blackbox Testing|black]] or [[Whitebox Testing|white box tests]]

It is considered best practice to write unit tests functions which are *non trivial*.
The problem hence is defining what is *non trivial*.
While approaches like [[Test Driven Development]] advocate, that every function is non trivial,
opponents of this methodology point out, that functions that only use build in functionality like:

```py
def add(x, y):
  return x + y
```

are probably not worth the amount of overhead that writing a tests occurs.

> **Test the function if:**
> 
> 1. The function is non-changing.
> 2. The function is used in multiple contexts.
> 3. Each iteration of the function is required to fulfill certain properties.
> 
> **Don't test functions if:**
> 
> 1. It is uncertain that the function will exist tomorrow.
> 2. You don't know if and where the function will be used.
> 3. The function is only composed of preexisting functions that were tested.
> 
> The reasons for testing **always** outweigh the reasons against testing. 

## Parameterization

Often unit tests can be grouped to reduce repetitive code.
Some testing frameworks offer this functionality out of the box like `NUnit`:

```cs
[Test]  
[TestCase(1, "1")]  
[TestCase(2, "2")]  
[TestCase(3, "Fizz")]  
// Further cases omitted for brevity  
public void FizzBuzz(int input, string expected)  
{  
Assert.AreEqual(expected, UnderTest.FizzBuzz(input));  
}
```

Similar functionality can be achieved in all [[Programming Language Overview.canvas|programming languages]].

```py
def my_test(self):
  data = [(1, "1"), (3, "Fizz"), (5, "Buzz")]
  for inpt, expected in data:
    self.assertEqual(expected, fizz_buzz(inpt))
```

_____

## Sources

- [Wikipedia - Unit testing](https://en.wikipedia.org/wiki/Unit_testing)
- [Kevlin Henney - Structure and Interpretation of Test Cases](https://youtu.be/MWsk1h8pv2Q?si=iLe_AGMzpZa19tTs)
- [Kevlin Henney - What we talk about when we talk about unit testing](https://youtu.be/-WWIeXmm4ec?si=hrV4mp7UahHq6Lb7)
- [Kevin Dill - Where The $@\*&% Are Your Tests?!](https://www.youtube.com/watch?v=IW5i9DjKT3U)