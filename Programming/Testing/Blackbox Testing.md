---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-12-19
tags: 
- testing
- testing-approach
- programming
---

# Blackbox Testing

[[Blackbox Testing]] is a [[Testing]] approach which **does not** make assumptions about the underlying implementation.

This means that *blackbox tests* (opposite to [[Whitebox Testing|white box tests]]) are independent of the underlying implementation.
Using black box tests at a high enough level of abstraction is preferable, since this will leave you the freedom of changing the underlying implementation to something better.

An example of what you want to test this way is the input system.
You don't know what your inputs will do, but you want your inputs to do *something*.
Hence you can set up easy [[Sanity Testing|smoke tests]] that give alarm if this is not the case.

[[Continuous Delivery#Continuous Testing|Continuous]] *black box* testing is the only testing approach used in [[Test Driven Development|TDD]].