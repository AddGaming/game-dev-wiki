---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-12-19
tags: 
- testing
- programming
- test-category
- deterministic
---

# Regression Testing

[[Regression Testing|Regression tests]] are [[Testing#Deterministic Tests|deterministic]] [[Whitebox Testing|white box]] tests written to ensure that the software doesn't exhibit unwanted behavior in the future.

By that definition, *regression test* can be realized as [[Acceptance Testing|acceptance]], [[Confirmation Testing|confirmation]], [[Sanity Testing|sanity]] or as an [[AB Testing|A/B test]].
While some can be written as [[Unit Testing|unit tests]], most regression tests will be [[System Testing|system tests]].

Given that you find a bug during manual testing of your game.
You should write a [[Confirmation Testing|confirmation test]] first to ensure that you can reproduce the observed bug.
Once you can reliably reproduce it, you can move on to fix the bug.
Now the [[Confirmation Testing|confirmation tests]] that you have written prior becomes a *regression test* that prevents you from reintroducing the given bug.

_____

## Sources

- [Xiao Li and Yingfeng Chen - Building An Intelligent Game Testing System in Netease MMORPG Games](https://www.youtube.com/watch?v=ohuNWkFjd7E)