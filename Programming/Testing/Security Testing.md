---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-12-19
tags: 
- testing
- programming
- test-category
- deterministic
---

# Security Testing

[[Security Testing|Security tests]] are [[Blackbox Testing|black]] or [[Whitebox Testing|white box]] tests meant to test possible attack vectors on a given system.
Security tests are [[Testing#Deterministic Tests|deterministic]] and mostly realized as [[System Testing|system tests]].

An simple example would be to test for an [SQL injection](https://en.wikipedia.org/wiki/SQL_injection) on an input field exposed to the user.

_____

## Sources

- [hackerone - What Is Security Testing](https://www.hackerone.com/knowledge-center/what-security-testing#:~:text=Security%20testing%20is%20an%20important,unauthorized%20access%2C%20and%20data%20breaches.)