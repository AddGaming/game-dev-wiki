---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2024-01-03
tags: 
- testing
- programming
- test-category
- deterministic
---

# System Testing

[[System Testing|System tests]] are [[Testing#Deterministic Tests|deterministic]] [[Blackbox Testing|blackbox tests]] that test a whole system instead of each part on its own.
System tests offer the most return of investment when writing tests since they test a wide range of code with only one test.
They are also defining a system in it's completeness while leaving the implementation unknown which enables greater flexibility in the code.
Good system tests are a better description of the system than any game design document could ever be.

## End to End Tests (E2E)

A special case of [[System Testing|system tests]] are *E2E tests*.
*E2E tests* test the system from the perspective of the end user in a realistic user environment.
Your *E2E tests* will hence represent the range of activities that your player be able to perform with the game.

While this is something *E2E tests* can be, they also can be implemented in a way that is [hindering a good development environment/culture](https://www.youtube.com/watch?v=QFCHSEHgqFE).

## Integration Tests

[[System Testing#Integration Tests|Integration tests]] test the interaction, the *integration*, of multiple systems together.
This necessitates that the tests know of the existence of system A and system B to perform the test, hence making them [[Whitebox Testing|whitebox tests]].
For that reason; *Integration tests* should be avoided. 
If it is specific for your system, then its covered by System tests.
Else the constituents form a system on their own. 
Hence you should make them their own system with a clearly defined interface to test against. 
Therefore making the *integration test*, a normal *system test* again.

_____

## Sources

- [Wikipedia - Integration testing](https://en.wikipedia.org/wiki/Integration_testing)
- [Wikipedia - System testing](https://en.wikipedia.org/wiki/System_testing)
- [Brian Provinciano - Automated Testing and Instant Replays in Retro City Rampage](https://www.youtube.com/watch?v=W20t1zCZv8M)
- [Dave Farley - Don't Do E"E Testing!](https://www.youtube.com/watch?v=QFCHSEHgqFE)