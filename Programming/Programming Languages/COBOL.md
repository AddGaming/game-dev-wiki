---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-26
tags: 
- programming
- language
- compiled
- missing
---

# COBOL

[[COBOL]] is a *procedural*, [[Code compilation|compiled]] programming language.

## Creating Games

*content*

### Porting

*content*

## Tooling and Ecosystem

*content*

## Example Projects

*content*

_____

## Sources

- [Wikipedia - COBOL](https://en.wikipedia.org/wiki/COBOL)
