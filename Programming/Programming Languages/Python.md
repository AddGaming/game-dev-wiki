---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-25
tags: 
- programming
- language
- MacOS
- Windows
- Linux
- interpreted
- high-level
---

# Python

[[Python]] is a [[High-Level programming language|high-level]], [[Code compilation|compiled]], [[Code interpretation|interpreted]] language that focuses on code readability.
The language provides tools to program in a [[Object Oriented Programming|object oriented]] style while using a reduced set of [[Functional Programming|functional]] tools.
The dominant [[programming paradigm|paradigm]] in [[Python]] is [[Data Driven Design|data driven design]].
The language-interpreter is available on Windows, MacOS and Linux.

Creating games in Python is generally discouraged, because the language is slow due to the [[Code interpretation|interpreted]] nature.

## Creating Games

While creating games is discouraged, that doesn't mean it is impossible.
Python has many available [[Game Library|game libraries]] to choose from.
Depending on the *libraries* you choose, you can target some platforms but loose access to another.
There is currently not one [[Programming Library|library]] or [[Game Engine|engine]] which can ship games to all major platforms.

Due to performance implications, it is recommended to use not all features of *Python*, but only a subset which runs somewhat performant.
This subset depends on the [[Code interpretation|interpreter]] you choose.

One way of increasing the performance of your *Python* game, is to not use *Python*.
*Python* provides multiple ways of interfacing with the language from or to other languages.
One of the most popular ways is [Cython](https://cython.org/) which provides *Python* like syntax 
while directly getting access to some [[C Language|C]] capabilities.

### Porting

Python can currently ship games to *PC*, *Web*, *Embedded Devices* and *Mobile*. 
There are three ways of shipping games written in Python.

#### Raw Python

If you ship the raw *Python* code, then this code will run on all environments that have a *Python* interpreter.
This includes *Windows*, *MacOS*, and many *Linux* distributions.
With this approach it is impossible to ship to mobile, or the web.

#### Using a [[Bundler]] or [[Code compilation|Compiler]]

When using a [[Bundler|bundler]], having *Python* installed is not a requirement anymore.
What most bundlers do, is combining your code with a version of your *Python [[Code interpretation|interpreter]]*.
This way the user has only one executable which he can run.
The most popular [[Bundler|bundler]] of *Python* is [`py-to-exe`](https://pypi.org/project/auto-py-to-exe/).

[[Code compilation|Compilation]] is what allows you to run your code on *Embedded Devices* and *Mobile*
For targeting *Mobile*, you want to use [Buildozer](https://buildozer.readthedocs.io/en/latest/) in combination with a UI [[Programming Library|library]] 
that can target mobile devices like [[Kivy]] or [[PyQt]].  
For targeting *Embedded*, you want to use [Circuit Python](https://circuitpython.org/).

#### Writing for the Web

If you are creating a web application, you will want to use a web-framework like [[Django]] or [[Flask]].
Both frameworks support [[HTML templating]].
If your web page needs interactive elements, consider using [[HTMX]] or [[PyScript]].

## Tooling and Ecosystem

Python has multiple libraries that one can use to make games.
This includes:

### Game Libraries

```dataview
table
choice(contains(row.tags, "Windows"), "✅", "❌") as Windows, 
choice(contains(row.tags, "MacOS"), "✅", "❌") as MacOs, 
choice(contains(row.tags, "Linux"), "✅", "❌") as Linux,
join(row.categories, ", ") as Categories,
join(row.features, ", ") as Features
from #game-library 
where contains(row.languages, "Python")
sort row.categories desc 
```

### Interpreter

| Interpreter                                   | Description                                                                                             |
| --------------------------------------------- | ------------------------------------------------------------------------------------------------------- |
| [C Python](https://www.python.org/downloads/) | The standard implementation of *Python* written in C                                                    |
| [PyPy](https://www.pypy.org/)                 | Due to [[JIT]], programs often run faster and consume less memory. But not all libraries are compatible |
| [MicroPython](https://micropython.org/)       | An interpreter that runs on microcontrollers                                                            |

### Compiler

| Interpreter                                              | Description   |
| -------------------------------------------------------- | ------------- |
| [Nuitka](https://www.nuitka.net/)                        | Python to C   |
| [Shedskin](https://github.com/shedskin/shedskin)         | Python to C++ |
| [Buildozer](https://buildozer.readthedocs.io/en/latest/) | Python to APK | 


### Testing

#### [[Unit Testing]]

The default testing framework included in most *Python* distributions is [unittest](https://docs.python.org/3/library/unittest.html).
If you need more features that are not available in [unittest](https://docs.python.org/3/library/unittest.html), consider [pytest](https://docs.pytest.org/).
A full list of *Python* testing frameworks can be found [here](https://wiki.python.org/moin/PythonTestingToolsTaxonomy):

#### [[System Testing]]

For testing web applications, [Selenium](https://selenium-python.readthedocs.io/) provides bindings for *Python*.
Other use cases require setting up a costume testing environment.

## Example Projects

*missing*

_____

## Sources

- [Wikipedia - Python](https://en.wikipedia.org/wiki/Python_(programming_language))
- [Python Website](https://www.python.org/)
- [Python Wiki - Python Game Libraries](https://wiki.python.org/moin/PythonGameLibraries)
- [YouTube - Clear Code](https://www.youtube.com/@ClearCode)
- [YouTube - DaFluffyPotato](https://www.youtube.com/@DaFluffyPotato)
- [Emery Berger - Python Performance Matters (Strange Loop 2022)](https://youtu.be/vVUnCXKuNOg?si=JAm8V1ik_7pCAv5R)
- [YouTube - anthonywritescode](https://www.youtube.com/@anthonywritescode)
- [YouTube - Dave's Space](https://www.youtube.com/@DavesSpace)
- [EuroPython Conference](https://www.youtube.com/@EuroPythonConference)
- [YouTube - mCoding](https://www.youtube.com/@mCoding)
