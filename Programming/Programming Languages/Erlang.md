---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-11-16
tags: 
- programming
- language
- BEAM
- MacOS
- Windows
- Linux
- interpreted
- high-level
- object-oriented
---

# Erlang

[[Erlang]] is a [[Code interpretation|interpreted]], [[Code compilation|compiled]], [[Object Oriented Programming|object oriented]], [[Concurrency|concurrent]] programming language running on the [[BEAM]].
The language is based on the idea of creating multiple lightweight disjoint *processes* that communicate with each other via message passing 
and that get managed by the [[Code interpretation|interpreter]]. 
The language is currently not recommended to be used to create game clients with a [[GUI]], yet is favorable for creating game servers.

## Creating Games

As many [[Programming Language Overview.canvas|Programming Language Overview|programming language]] [running on the BEAM](https://github.com/llaisdy/beam_languages#34-languages-on-the-beam),
*Erlang* is suited for long running reliable services.
This is exactly whats needed from game servers.

The adoption of *Erlang* in a technology stack for a game production depends hence on two other factors: 
Does your team have the expertise to adopt the language and can you invest the additional time needed to port the core game logic needed onto the server?

That said, *Erlang* has no special libraries currently to support any kind of game development.
While the creation of [[HTTP]] clients and servers is [directly supported in the language](https://www.erlang.org/doc/apps/inets/http_server.html), 
it is generally only recommended for people who already have experience with the web.
But if you are experienced, you can create reliable scaling servers quicker than in any other language. 

> "If Java is 'write once, run anywhere', then Erlang is 'write once, run forever'."
>  \- Joe Armstrong

### But I really really want a *Erlang* Client

[[Erlang]] offers a [[FFI]] for [[C Language|C]] called `Nativly Implemented Functions` ([NIFs](https://www.erlang.org/doc/tutorial/nif.html)).
So you could implement functions that interface with the hardware by hand using them.

For graphics one should consider creating a [C-node](https://erlang.org/documentation/doc-5.4/doc/tutorial/cnode.html).
That way your graphics interface looks like a normal [[Erlang]] process to the language,
while you as the programmer can interface with common [[C Language|C]] libraries for creating and updating the window.

## Porting

To run [[Erlang]] programs one needs the associated [[Code interpretation|VM]].
The [[BEAM]] currently supports building from scratch and a pre-build Windows, MacOS and Linux installer. 
This means that it is currently not possible to ship games to *Mobile* or *Console* with Erlang.
If you want to host a game server with [[Erlang]], consider using a *Linux* machine.

## Tooling and Ecosystem

In theory any library or tool that works for languages like [[Elixir]], [[Elm]] or other [[BEAM]] languages,
should also be available for development in [[Erlang]] since they all compile to the same [[Byte Code]].

A community list of libraries can be found [here](https://github.com/erlang-punch/awesome-erlang).

## Example Projects

*coming soon*

_____

## Sources

- [Erlang - Website](https://www.erlang.org/downloads)
- [The Mess We're In - Joe Armstrong](https://youtu.be/lKXe3HUG2l4?si=WvezK0zmnaUAsVuY) 
- [Systems that run forever self-heal and scale - Joe Armstrong](https://youtu.be/cNICGEwmXLU?si=K6nkbyYw9-cUCYRJ)
- [Erlang Solutions - Youtube](https://www.youtube.com/@ErlangSolutions)
- [Erlang - Wikipedia](https://en.wikipedia.org/wiki/Erlang_(programming_language))
