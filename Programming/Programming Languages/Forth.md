---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-23
tags: 
- programming
- language
- compiled
- high-level
- stack-based
- missing
---

# Forth

[[Forth]] is a [[Stack Based Programming Language|stack based]], [[Code compilation|compiled]] programming language.

## Creating Games

*content*

### Porting

*content*

## Tooling and Ecosystem

*content*

## Example Projects

*content*

_____

## Sources

- [Wikipedia - Forth](https://en.wikipedia.org/wiki/Forth_(programming_language))
