---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-26
tags: 
- programming
- language
- MacOS
- Windows
- Linux
- interpreted
- high-level
- missing
- jvm
---

# Kotlin

[[Kotlin]] is a [[High-Level programming language|high-level]], [[Code compilation|compiled]], [[Code interpretation|interpreted]] programming language running on the [[JVM]].

## Creating Games

*content*

### Porting

*content*

## Tooling and Ecosystem

*content*

## Example Projects

*content*

_____

## Sources
