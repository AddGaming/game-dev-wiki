---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-23
tags: 
- programming
- language
- MacOS
- Windows
- Linux
- Web
- compiled
- low-level
- missing
---

# Rust

[[Rust]] is a [[Code compilation|compiled]], [[Low-Level programming language|low level]] programming language.

## Creating Games

*content*

### Porting

*content*

## Tooling and Ecosystem

*content*

## Example Projects

*content*

_____

## Sources

- [YouTube - fasterthanlime](https://www.youtube.com/@fasterthanlime)
- [YouTube - Jon Gjengset](https://www.youtube.com/@jonhoo)
- [YouTube - Let's Get Rusty](https://www.youtube.com/@letsgetrusty)
- [YouTube - Logan Smith](https://www.youtube.com/@_noisecode)
