---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-25
tags: 
- programming
- language
- Linux
- interpreted
- high-level
- object-oriented
- missing
---

# Smalltalk

[[Smalltalk]] is [[Object Oriented Programming|object oriented]], [[High-Level programming language|high level]] programming language.

## Creating Games

*content*

### Porting

*content*

## Tooling and Ecosystem

*content*

## Example Projects

*content*

_____

## Sources

- [Wikipedia - Smalltalk](https://en.wikipedia.org/wiki/Smalltalk#:~:text=Smalltalk%20is%20a%20purely%20object,Smalltalk)
- [GNU Smalltalk Website](https://www.gnu.org/software/smalltalk/)
