---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-26
tags: 
- programming
- language
- Windows
- high-level
- compiled
- missing
---

# Pascal

[[Pascal]] is a [[High-Level programming language|high-level]], [[Code compilation|compiled]], programming language.

## Creating Games

*content*

### Porting

*content*

## Tooling and Ecosystem

*content*

## Example Projects

*content*

_____

## Sources
