---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-23
tags: 
- programming
- language
- interpreted
- high-level
- stack-based
- missing
---

# Uiua

[[Uiua]] is a [[Stack Based Programming Language|stack based]], [[Code interpretation|interpreted]] programming language.

## Creating Games

*content*

### Porting

*content*

## Tooling and Ecosystem

*content*

## Example Projects

*content*

_____

## Sources

- [Uiua - Website](https://www.uiua.org/)
