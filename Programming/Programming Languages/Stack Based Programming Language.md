---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-26
tags: 
- programming
- stack-based
- meta
- missing
---

# Stack Based Programming Language

A [[Stack Based Programming Language]] is a language that operates on a stack as its primary model of representing computation.

## List of stack-based programming languages

```dataview
List
from #stack-based
where file.name != this.file.name
```

_____

## Sources
