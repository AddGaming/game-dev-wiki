---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-25
tags: 
- programming
- language
- MacOS
- Windows
- Linux
- Mobile
- Playstation
- Xbox
- compiled
- low-level
- missing
---

# C

[[C Language|C]] is a [[Code compilation|compiled]], [[Low-Level programming language|low level]] programming language.

## Creating Games

*content*

### Porting

*content*

## Tooling and Ecosystem

*content*

## Example Projects

*content*

_____

## Sources

- [Programming in Modern C with a Sneak Peek into C23 - Dawid Zalewski](https://youtu.be/lLv1s7rKeCM?si=Wf1ttKGg14thP7cY)
