---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-24
tags: 
- programming
- language
- MacOS
- Windows
- Linux
- Mobile
- interpreted
- high-level
- missing
- jvm
---

# Java

[[Java]] is a [[Code interpretation|interpreted]] programming language running on the [[JVM]].

## Creating Games

*content*

### Porting

*content*

## Tooling and Ecosystem

*content*

## Example Projects

*content*

_____

## Sources

