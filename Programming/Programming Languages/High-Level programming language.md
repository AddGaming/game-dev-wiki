---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-24
tags: 
- programming
- high-level
- meta
---

# High-level programming language

A [[High-Level programming language]] is a language that encourages to programmer to avoid low memory control structures and work on higher levels of abstractions.
Hence, many such languages automate many processes such as memory allocation and freeing as well as implementing many protocols for the programmer to use ([[Batteries included language]]).
This contrasts with [[Low-Level programming language|low-level programming languages]] which explicitly provide and encourage taking control of low level primitives.

## The spectrum

The term *"high-level"* is relative and thus, the meaning of *"high-level programming language"* is also always changing.
The accepted lower end for *high-level languages* in the public discourse are currently languages like [[Java]] or [[Pascal]].
Taking them as the baseline, the term currently includes languages like...
- ...[[Java]] that allow for low level control, but discourage that in daily use.
- ...[[Python]] which does not allow for low level control, even tho it could be represented inside the language
- ...[[APL]] which has no way of representing low level controls inside it. 

The first *high-level* languages were languages like [[C Language|C]] , [[COBOL]] or [[Fortran]].
But those languages are nowadays recognized as [[Low-Level programming language|low level]] by most programmers. 

## For game development

Game development in such languages is possible, but discouraged for bigger projects.
Lets take [[Python]] and [[C Language|C]] as examples.
Both have game libraries and game engines available and both *can* be used to make games from scratch.
For simple games, you will not notice a difference, given, that you are proficient in both languages.
But depending on the benchmark, [[Python]] is on average 100x slower that C.
That can be the difference between rendering in 1000 ms (1 [[FPS]]) or 10 ms (100 [[FPS]]).

Since games are often very performance critical applications, *high-level* languages often rely on [[Programming Library|libraries]] to compute efficiently.
Those libraries are often written inside [[Low-Level programming language|low level languages]] and are thus more performant.
But calling code from another language has performance panelties.
In well written and performance optimized python code, you will spend most of your time, translating data between library functions and [[Python]] itself.
You will also be hampered in your ability to write your own functions, since the moment you loop in [[Python]], all performance is lost.

In the beginning of a game project you hence must make the decision, if you will need the performance down the line and which part of your game will be performance critical.
You can read more about that [[Common performance bottle necks in games|in this article]].

## List of high-level programming languages

```dataview
List
from #high-level 
where file.name != this.file.name
```

_____

## Sources

- [Wikipedia - High-level programming langue](https://en.wikipedia.org/wiki/High-level_programming_language)
- [Wikipedia - Very high-level programming language](https://en.wikipedia.org/wiki/Very_high-level_programming_language)
- [GeeksForGeeks - difference between high level and low level languages](https://www.geeksforgeeks.org/difference-between-high-level-and-low-level-languages/)
- [Bluebird - high level programming languages](https://bluebirdinternational.com/high-level-programming-languages/)
- [Wikipedia - List of programming languages by type](https://en.wikipedia.org/wiki/List_of_programming_languages_by_type)
- [Benchmarksgame - python vs gcc](https://benchmarksgame-team.pages.debian.net/benchmarksgame/fastest/python3-gcc.html)
