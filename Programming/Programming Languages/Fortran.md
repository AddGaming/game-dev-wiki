---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-26
tags: 
- programming
- language
- missing
---

# Fortran

[[Fortran]] is a *procedural*, [[Code compilation|compiled]] programming language.

## Creating Games

*content*

### Porting

*content*

## Tooling and Ecosystem

*content*

## Example Projects

*content*

_____

## Sources

- [Wikipedia - Fortran](https://en.wikipedia.org/wiki/Fortran)
