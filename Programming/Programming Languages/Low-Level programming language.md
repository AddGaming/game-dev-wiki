---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-24
tags: 
- programming
- missing
- meta
- low-level
---

# Low-level programming language

A [[Low-Level programming language]] is a language that encourages the programmer to take control over memory structures and computational procedures.
These languages are typically characterized by having very little pre-build functionality and rely heavily on the programmer to re implement the features that he wants or use a external library.
This contrasts with [[Low-Level programming language|low-level programming languages]] that discourage this behavior.



## The spectrum

## List of high-level programming languages

```dataview
List
from #low-level 
where file.name != this.file.name
```

_____

## Sources
