---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-27
tags: 
- programming
- language
- MacOS
- Windows
- Linux
- compiled
- low-level
- missing
---

# C++

[[C++]] is a [[Code compilation|compiled]], [[Low-Level programming language|low level]] programming language.

## Creating Games

*content*

### Porting

*content*

## Tooling and Ecosystem

*content*

## Example Projects

*content*

_____

## Sources

