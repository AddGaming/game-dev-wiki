---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-11-22
tags: 
- missing
- BEAM
- programming
- language
- MacOS
- Windows
- Linux
- interpreted
- high-level
- object-oriented
---

# Efene

[[Efene]] is a [[Code interpretation|interpreted]], [[Code compilation|compiled]], [[Object Oriented Programming|object oriented]], [[Concurrency|concurrent]] programming language running based on [[Erlang]] running on the [[BEAM]].
The language is an alternative syntax for the [[Erlang]] programming language.
[[Efene]] is currently [*"finished"*](https://www.reddit.com/r/erlang/comments/ymrvq2/anyone_using_efene/) and doesn't receive new updates. 
Same as [[Erlang]], the language is currently not recommended to be used to create game clients with a [[GUI]], yet is favorable for creating game servers.

For more information about game development in *Efene*, visit the [[Erlang]] page.

_____

## Sources

- [Efene - Website](https://efene.org/index.html)
- [Efene - GitHub](https://github.com/efene/efene)
