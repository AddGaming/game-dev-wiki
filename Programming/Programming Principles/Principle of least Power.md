---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-20
tags: 
- programming
- principle
---


# Least Power

***Choosing the least powerful tool suitable for a given purpose.***

The *least power [[Programming Principle|principle]]* or *least power rule* is meant to prevent security flaws and improve work flows.
It has close relations with the [[Principle of least privilege|principle of least privilege]] and the [[single responsibility principle]].

## Motivation

### Speed

Let's say you want to edit a text file quickly.
Yes, you could open word, scroll with your mouse down to page 3 and den make the 1 character change.
Or you could use a tool like [[Vim]] from the command line and be done with the change faster than word even finished booting.  

Choosing the least powerful tool for a task is often the fastest way to accomplish a given goal.

### Security

In your daily life, not using an account with administrative access on your computer is advisable.
All you need to do is *use* software, not *installing* software.
If you then accidentally download a malicious software from a link, the damage that this software can cause is severely limited, since the active account lacks the necessary rights.

Choosing the least powerful tool for a task often prevents security problems before they can arise.

_____

## Sources

- [Wikipedia - Rule of least power](https://en.wikipedia.org/wiki/Rule_of_least_power)
- [W3 - The Rule of Least Power](https://www.w3.org/2001/tag/doc/leastPower.html)