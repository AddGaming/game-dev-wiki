---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-20
tags: 
- programming
- principle
---


# Single-Responsibility

***A thing should do one thing and that one thing well.***

The *single-responsibility [[Programming Principle]]*  is meant to encourage good design 
by reducing the [[Interface|interface]] and range of use cases that have to be considered while designing.

## Motivation 

### Size

Consider a simple feature; lets say searching a word in a piece of text.
The scope of that is so small, that every programmer can create this software without much problems.

Imagine having to write a tool that can filter, edit, recompose and much more and do everything correct.
That is a huge task.
But just writing a simple peace that just does one thing? - That's an afternoon at most.

Since the size is so small, the amount of errors that can occur is also reduced.

### Compose-ability

Consider the word-searcher from above.
Adding new ways to use that tool is now easy: 
You want the position of the character in the text as output? - The tool got you covered.
You want the sentences in which the word occurred? - Also fine.

Those are all simple things to add, since the tool is just doing one thing.
But we can now choose a second piece of software to handle another operation and another and another.
That's the concept of a [pipe](https://de.wikipedia.org/wiki/Pipeline_(Unix)).

## Alternative definitions

- [Robert C. Martin](https://en.wikipedia.org/wiki/Robert_C._Martin) 2018: *"A module should be responsible to one, and only one, actor."*
- [Robert C. Martin](https://en.wikipedia.org/wiki/Robert_C._Martin) 2003: *"A class should have only one reason to change."*

_____

## Sources

- [Wikipedia - Single-responsibility principle](https://en.wikipedia.org/wiki/Single-responsibility_principle)