---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-20
tags: 
- programming
- meta
- missing
---

# Code compilation

[[Code compilation|Compilation]] is the process of transforming instructions given in a human readable format into a format 
that can be executed by a different entity.
This is often the machines CPU, but can also be a different [[Code interpretation|interpretor]].
This differs from transpilation, which translates the code from one language to another.

The compilation process is often extendable to run other scripts before, during or after the code got compiled.

## Advantages

*content*

## Disadvantages

*content*


## List of programming languages that compile to native code

```dataview
list from #programming and #language and #compiled
```


_____

## Sources

- [Wikipedia - Compiler](https://en.wikipedia.org/wiki/Compiler)
- [Wikipedia - Source-to-source compiler](https://en.wikipedia.org/wiki/Source-to-source_compiler)
