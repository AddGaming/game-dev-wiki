---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-19
tags: 
- programming
- meta
---

# Code Interpretation

An [[Code interpretation|Interpreter]] is a program that runs a specified instruction set.
This enables the developer of such interpreters to allow programs for this interpreter, 
that rely on commands not found in the hardware the program is running on.

## Advantages

- Porting of software to different platforms becomes as easy, as installing the interpreter on those platforms.

## Disadvantages

- Since the code needs to be interpreted before it is executed, interpreters will always be slower than code compiled to machine instructions.


## List of programming languages that have a interpreter

```dataview
list from #programming and #language and #interpreted 
```

_____

## Sources

- [Wikipedia - Interpreter](https://en.wikipedia.org/wiki/Interpreter_(computing))
