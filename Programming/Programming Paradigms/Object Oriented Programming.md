---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-25
tags: 
- programming
- design
- paradigm
---

# Object-Oriented Programming

The term *[[Object Oriented Programming|object-oriented]]* was coined by Alan Kay in 1966 and describes a certain way to structure programs,
with the goal to reduce complexity.
According to him, *OOP* means: ***"messaging, local retention and protection and hiding of state-process, and extreme late binding of all things."***

An *object-oriented* programming language, is a language that support the *object-oriented* ways of structuring programs.

## What OOP is not

### Confusion between object-oriented and data driven

Modeling your data is a necessary condition to program *object-oriented*, but it is not a sufficient condition.
You are doing [[Data Driven Design]], when you are modeling your data and then work procedural on that data.

### Dry

The articles there put heavy emphasis on "`constructing reusable chuncs of code`" and state that this is one of the most important aspects.
This is not at the core of *OOP*.
*OOP* is an attempt to reduce complexity of systems, but reusing code increases coupling and with that increases complexity.
This is why *`"extreme late binding"`* has the word *`"extreme"`* in front of it.
Code reuse is not antithetical to object orientation, and it should be on every programmers mind.
But it is **not** at the heart of *OOP*.

### A Programming Language Feature

There is a reason why *OOP* has the rank of paradigm and is not a simple category of programming languages.
You can write *object-oriented* code in many programming languages that do not provide utilities to facilitate that.
Hence languages fall on a spectrum on how much they **facilitate** *OOP*, 
but programming in one of those languages, doesn't make your program *object-oriented*.

Languages like [[Forth]], or [[Uiua]] do not provide any way of representing objects.
Hence it is impossible to program *object-oriented* in them.

Procedural languages like [[Rust]] have structs and other composite data types 
and hence allow for the programmer to construct the other needed pieces by them selves.
By that notion is [[C Language|C]] as much a *object-oriented* language as [[Java]].
Yes, that means most of the marketing for [[Java]] or [[C++]] has been false!

[[Kotlin]] is one step ahead in that regard providing you primitives that allow you more easily to structure programs object oriented.
It also allows you to profit from the inherent parallelization that is possible with such data models, thanks to a good thread model. 

On the far end on the spectrum we have languages like [[Smalltalk]] and [[Erlang]] that fulfill all points from the definition per default 
and make it very easy to write programs in that style.


## List of Object-Oriented Languages

```dataview
list from #object-oriented and #language 
```

## Alternative definitions

### [[Smalltalk]]

1. Everything is an object.
2. Objects communicate by sending and receiving messages (in terms of objects).
3. Objects have their own memory (in terms of objects).
4. Every object is an instance of a class (which must be an object).
5. The class holds the shared behavior for its instances (in the form of objects in a program list)
6. To eval a program list, control is passed to the first object and the remainder is treated as its message.

_____

## Sources

- [C2 Wiki - Alan Kays Definition Of Object Oriented](https://wiki.c2.com/?AlanKaysDefinitionOfObjectOriented)
- [Agile Architecture Part 1 - Allen Holub](https://youtu.be/0kRCFVGpX7k?si=x2FRcmwAu2C6tWah)
- [Seminar with Alan Kay on Object Oriented Programming](https://youtu.be/QjJaFG63Hlo?si=5xld7KRgIZaWAy52)
- [Wikipedia - Object-oriented programming](https://en.wikipedia.org/wiki/Object-oriented_programming)
- [Email between Stefan Ram and Alan Kay](https://gist.github.com/mattpodwysocki/1012287)
- [Luca Cardelli - Everything is an Object](http://lucacardelli.name/Talks/1997-06%20Everything%20is%20an%20Object%20%28ECOOP%20Prototypes%20Workshop%29.pdf)
- [Ovid - Alan Kay and OO Programming](https://ovid.github.io/articles/alan-kay-and-oo-programming.html)
