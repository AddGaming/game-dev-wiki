---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-25
tags: 
- programming
- design
- paradigm
- missing
---

# Functional Programming

## List of Functional Languages

```dataview
list from #functional and #language 
```

_____

## Sources
