---
authors: 
- ["Raphael Diener", "https://twitter.com/diener_raphael"]
last-edit: 2023-10-19
tags: 
- programming
- meta
- missing
---

# Programming Paradigm

## List of Programming Paradigms

```dataview
list 
from #programming and #paradigm 
```